﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSample;

public class RequestQueryCommand
{
	/// <summary>
	/// 요쳥됨 쿼리 명령어
	/// </summary>
	public string queryCommand;

	/// <summary>
	/// NonQuery 방식이 아닌 경우 명령어 실행 이후 발생시킬 이벤트
	/// </summary>
	public System.Func<MySqlDataReader, Task> onExecuteReader;

	public bool isNonQuery => onExecuteReader == null;

	public RequestQueryCommand(
		string command, 
		System.Func<MySqlDataReader, Task> onExecuteReader)
	{
		queryCommand = command;
		this.onExecuteReader = onExecuteReader;
	}



}
