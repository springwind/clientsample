﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    /// <summary>
    /// 새로운 채팅 요청 패킷을 나타내기 위한 구조체
    /// </summary>
    public struct NewChattingContext
    {
        /// <summary>
        /// 발신자 닉네임
        /// </summary>
        public string nickname;

        /// <summary>
        /// 내용
        /// </summary>
        public string context;


        public NewChattingContext(string nickname, string context)
        {
            this.nickname = nickname;
            this.context = context;
        }

    }
}
