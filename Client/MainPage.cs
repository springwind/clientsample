﻿using Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
	public partial class MainPage : Form
	{
		public MainPage()
		{
			InitializeComponent();
			Button_Login.Click += OnLoginButtonClicked;
            Button_Register.Click += OnRegisterButtonClicked;

		}


		private async void OnLoginButtonClicked(object sender, EventArgs e)
		{
			// ID/PW 에 작성된 내용을 얻습니다.
			string idText = TextBox_ID.Text;
			string pwText = TextBox_PW.Text;

			// 보낼 내용을 생성합니다.
			LoginRequestPacket loginRequest = new(idText, pwText);
			await Packet.SendAsync(ClientInstance.instance.connectedServerSocket,
				PacketType.LoginRequest, loginRequest);

			// 로그인 결과 확인
			await Packet.ReceiveAsyncPacketType(ClientInstance.instance.connectedServerSocket);
			LoginResponsePacket loginResponse = 
				await Packet.ReceiveAsyncHeaderAndData<LoginResponsePacket>(
				ClientInstance.instance.connectedServerSocket);

			// 로그인 성공
			if(loginResponse.result)
			{
				// 로그인된 유저 닉네임 기록
				ClientInstance.instance.thisUserNickname = loginResponse.nickname;

				new ChattingPage().Show();
				Visible = false;
			}
            // 로그인 실패
			else
            {
				MessageBox.Show("로그인에 실패하였습니다. \n ID 혹은 PW 를 확인하세요", "로그인 결과");
            }
        }

		private void OnRegisterButtonClicked(object sender, EventArgs e)
		{
			// 창 띄우기

			// 모달 다이얼로그
			// - 열린 창만 컨트롤할 수 있습니다.
			// 모달리스 다이얼로그
			// - 열린창 이외에 다른 창을 컨트롤할 수 있습니다.

			RegisterPage registerWindow = new RegisterPage();
			registerWindow.ShowDialog();
		}
	}
}
