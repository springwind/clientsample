﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client;

/// <summary>
/// 클라이언트가 실행되는동안 단 하나만 존재하게 될 객체입니다.
/// </summary>
public class ClientInstance
{
	/// <summary>
	/// 클라이언트 객체를 나타냅니다.
	/// </summary>
	public static ClientInstance instance { get; private set; }

	/// <summary>
	/// 연결된 서버 소켓
	/// </summary>
	public Socket connectedServerSocket { get; private set; }

	/// <summary>
	/// 로그인된 유저 닉네임
	/// </summary>
	public string thisUserNickname { get; set; }


	public static void InitializeClientInstance()
	{
		instance = new();
	}

	/// <summary>
	/// 서버에 접속합니다.
	/// </summary>
	/// <returns></returns>
	public async Task ConnectAsync()
	{
		IPEndPoint endPoint = new(
			//IPAddress.Parse(Constants.ADDRESS), //로컬호스트
			IPAddress.Parse("192.168.3.90"),   //강사님 서버
			Constants.SERVERPORT);
		connectedServerSocket = new(
			AddressFamily.InterNetwork, 
			SocketType.Stream, 
			ProtocolType.Tcp);

		// 연결 대기
		await connectedServerSocket.ConnectAsync(endPoint);
		ThreadPool.QueueUserWorkItem(OnServerConnected, connectedServerSocket);


	}

	/// <summary>
	/// 서버와 연결되었을 경우 호출되는 메서드
	/// </summary>
	/// <param name="sender"></param>
	private async void OnServerConnected(object sender)
	{
		Console.WriteLine("서버에 연결되었습니다.");

		//while (true)
		//{ 
		//
		//
		//}
	}

}


