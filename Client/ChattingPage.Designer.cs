﻿namespace Client
{
    partial class ChattingPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            TextBox_Send = new System.Windows.Forms.TextBox();
            ListBox_Users = new System.Windows.Forms.ListBox();
            Button_Send = new System.Windows.Forms.Button();
            TextBox_Chatting = new System.Windows.Forms.TextBox();
            SuspendLayout();
            // 
            // TextBox_Send
            // 
            TextBox_Send.Location = new System.Drawing.Point(0, 424);
            TextBox_Send.Name = "TextBox_Send";
            TextBox_Send.Size = new System.Drawing.Size(644, 23);
            TextBox_Send.TabIndex = 1;
            // 
            // ListBox_Users
            // 
            ListBox_Users.FormattingEnabled = true;
            ListBox_Users.ItemHeight = 15;
            ListBox_Users.Location = new System.Drawing.Point(650, 0);
            ListBox_Users.Name = "ListBox_Users";
            ListBox_Users.Size = new System.Drawing.Size(145, 424);
            ListBox_Users.TabIndex = 2;
            // 
            // Button_Send
            // 
            Button_Send.Location = new System.Drawing.Point(650, 423);
            Button_Send.Name = "Button_Send";
            Button_Send.Size = new System.Drawing.Size(145, 23);
            Button_Send.TabIndex = 3;
            Button_Send.Text = "전송";
            Button_Send.UseVisualStyleBackColor = true;
            // 
            // TextBox_Chatting
            // 
            TextBox_Chatting.Location = new System.Drawing.Point(0, 0);
            TextBox_Chatting.Multiline = true;
            TextBox_Chatting.Name = "TextBox_Chatting";
            TextBox_Chatting.ReadOnly = true;
            TextBox_Chatting.Size = new System.Drawing.Size(644, 424);
            TextBox_Chatting.TabIndex = 4;
            // 
            // ChattingPage
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(796, 450);
            Controls.Add(TextBox_Chatting);
            Controls.Add(Button_Send);
            Controls.Add(ListBox_Users);
            Controls.Add(TextBox_Send);
            Name = "ChattingPage";
            Text = "ChattingPage";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.TextBox TextBox_Send;
        private System.Windows.Forms.ListBox ListBox_Users;
        private System.Windows.Forms.Button Button_Send;
        private System.Windows.Forms.TextBox TextBox_Chatting;
    }
}